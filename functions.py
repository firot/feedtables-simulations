import cobra
import pandas as pd
from bs4 import BeautifulSoup
import requests
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from pathlib import Path
import matplotlib.pyplot as plt


class Render(QWebEngineView):
    def __init__(self, url):
        self.html = None
        self.app = QApplication(sys.argv)
        QWebEngineView.__init__(self)
        self.loadFinished.connect(self._loadFinished)
        self.load(QUrl(url))
        self.app.exec_()

    def _loadFinished(self, result):
        self.page().toHtml(self.callable)

    def callable(self, data):
        self.html = data
        self.app.quit()


class feed_frame:
    def __init__(self):
        """
        Her kunne et argument være conversion_table='conversion.csv'
        """
        
        self.conversion = self.conversion_table()
        feedtable_path = Path(__file__).parent.joinpath("feedtable.csv")
        if feedtable_path.exists():
            self.frame = pd.read_csv(feedtable_path)
        else:
            self.name_id = self.get_feedtable_ids()
            self.frame = self.get_frame()
            self.frame.to_csv(feedtable_path)
        self.frame = self.bigg_feed_table(self.frame, self.conversion.met).fillna(0)
        sustainability_path = Path(__file__).parent.joinpath("sustainability.csv")
        if sustainability_path.exists():
            self.sustainability = pd.read_csv(
                sustainability_path,
                index_col="Parameter",
            )
        else:
            self.sustainability = self.sustainability_table()
            self.sustainability.to_csv("sustainability.csv")

    def get_feedtable_ids(self):
        """
        Returns dict with {name: id} for feeds in the feedtables database
        """
        url = "https://www.feedtables.com/content/table-feed-profile?feed_pr_id=12558"
        html_source = Render(url).html
        soup = BeautifulSoup(html_source, "html.parser")
        name_id = dict()
        for name_list in soup.find_all("option"):
            text, val = name_list.text, name_list["value"]
            if val.isnumeric():
                name_id[text] = val
        return name_id

    def get_frame(self):
        """
        Download all feed tables and combine to dataframe

        """
        feed_frame = pd.DataFrame()
        for feed_name, feed_id in self.name_id.items():
            table = self.get_feed_table(feed_id)
            table = self.scale_feed_components(table[0]).set_index("Parameter")
            try:
                feed_frame[feed_name] = table["As fed"]
            except ValueError:
                continue
        feed_frame.columns = feed_frame.columns.str.strip()
        return feed_frame

    def scale_feed_components(self, feed: pd.DataFrame):
        """Scale feed components to g/kg"""
        for i, row in feed.iterrows():
            val, unit = self.to_g_per_kg(row["As fed"], row["Unit"])
            feed.at[i, "As fed"] = val
            feed.at[i, "Unit"] = unit
        return feed

    def get_feed_table(self, feed_id):
        u = f"https://www.feedtables.com/content/table-feed-profile?feed_pr_id={feed_id}"
        return pd.read_html(u)

    def sustainability_table(self):
        name_id = self.get_feedtable_ids()
        url = "https://www.feedtables.com/content/table-feed-profile?feed_pr_id="
        feedtables = {
            name: pd.read_html(f"{url}{number}") for name, number in name_id.items()
        }
        df = pd.DataFrame(
            {key: val[1]["As fed"] for key, val in feedtables.items() if len(val) > 1}
        )
        df["Parameter"] = feedtables["  Bakery byproduct "][1].Parameter
        df = df.set_index("Parameter")
        df.columns = df.columns.str.strip().str.replace(" ", "_")
        return df

    def to_g_per_kg(self, value, unit):
        scaler = {
            "%": 10,
            "g/kg": 1,
            "µg/kg": 0.000001,
            "mg/kg": 0.001,
            "1000 IU/kg": 0,
        }
        try:
            return (value * scaler[unit], "g/kg")
        except KeyError:
            return value, unit

    def conversion_table(
        self, table_path=Path(__file__).parent.joinpath("conversion.csv")
    ):
        table = pd.read_csv(table_path)
        return table[~table.met.isna()].set_index("Parameter")

    def bigg_feed_table(self, feed_table: pd.DataFrame, conversion: pd.DataFrame):
        feed_table = feed_table.merge(conversion, on="Parameter")
        feed_table = feed_table.set_index("met")
        return feed_table

    def add_feed_to_frame(self, recipe: pd.Series) -> pd.Series:
        """
        Add a new feed to the feed frame
        """
        self.frame[recipe.name] = self.feed_from_recipe(recipe)
        self.frame = self.frame.fillna(0)

    def feed_from_recipe(self, recipe: pd.Series):
        """
        Combine feed ingredients to compose diets.
        Returns the diet as pd.Series, and adds it as a column in the feed-dataframe
        """
        recipe.fillna(0)
        recipe = recipe[
            [ingredient in self.frame.columns for ingredient in recipe.index]
        ]
        scaled_ingredients = self.frame[recipe.index] * recipe
        feed = scaled_ingredients.T.sum()
        feed.name = recipe.name
        return feed

def simulation_list(model, feed):
    """
    Iterate over limiting metabolites using pfba.

        Parameters:
            model (cobra.core.Model): a metabolic model
            n (int): number of iterations
            feed (pd.Series): Amino acid composition
            enable_carbon (bool):
            step_size (float): allowed increment of limiting metabolite

        Returns:
            sols (list): list of cobra.core.solution
    """
    for e in model.exchanges:
        if e.id == "EX_o2_e":
            continue
        disable_import(e)
    feed_name = feed.name.replace(" ", "_")
    add_feed_supply_reaction(model, feed)
    model.reactions.get_by_id(feed_name).bounds = 0, 1
    return model.optimize()


def add_feed_supply_reaction(model, feed, scaled=False):
    """
    Define reaction with provided AA composition.

        Parameters:
            model (cobra.core.Model): metabolic model
            feed (pd.Series): metabolite composition
    """
    feed_name = feed.name.replace(" ", "_")
    rxn = cobra.Reaction(id=feed_name, name=feed_name)
    model.add_reactions([rxn])
    rxn.annotation["feedtables"] = feed.name
    rxn.bounds = (0.0, 1000.0)
    feed_mass_scaler = 1000 / feed.sum() if scaled else 1
    for m, val in feed.items():
        met_list = [
            m,
        ]
        for a in met_list:
            a += "_e"
            rxn.add_metabolites(
                {
                    model.metabolites.get_by_id(a): gram_to_mol(
                        model, a, val * feed_mass_scaler
                    )
                }
            )
    return feed_name


def get_feedtable_reactions(model):
    return [r for r in model.reactions if r.annotation.get("feedtables")]


def gram_to_mol(model, met_id, mass):
    """
    Convert amount in grams of metabolite to amount in mol of metabolite

        Parameters:
            model (cobra.core.Model): metabolic model
            met_id (str): name of metabolite
            mass (float): amount of metabolite in grams

        Returns:
            float: amount of metabolite in mol
    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> gram_to_mol(test, '2pg_c', 1)
    0.005463483087058729
    """
    return mass / model.metabolites.get_by_id(met_id).formula_weight


def disable_import(rxn):
    """
    Disable import but not export

        Parameters:
            rxn (cobra.core.Reaction)
    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> disable_import(test.reactions.EX_co2_e)
    >>> test.reactions.EX_co2_e.lower_bound
    0.0
    >>> disable_import(test.reactions.ACALD)
    >>> test.reactions.ACALD.lower_bound
    -1000.0
    """
    if not is_import(rxn):
        return
    if rxn.reactants and rxn.lower_bound:
        rxn.lower_bound = 0.0
    if rxn.products and rxn.upper_bound:
        rxn.upper_bound = 0.0


def is_import(rxn):
    """
    Returns 1*import direction for import reactions, otherwise 0

        Parameters:
            rxn (cobra.core.Reaction)

        Returns:
            -1 or 1 or 0: direction of import (0 means it cannot import)

    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> is_import(test.reactions.ACALD)
    0
    >>> is_import(test.reactions.EX_glu__L_e)
    1
    >>> test.reactions.EX_glu__L_e.upper_bound = 0.0
    >>> is_import(test.reactions.EX_glu__L_e)
    0
    """
    if len(rxn.metabolites) == 1 and rxn.compartments == {"e"}:
        makes_products = bool(rxn.products and rxn.upper_bound)
        makes_reactants = bool(rxn.reactants and rxn.lower_bound)
        return makes_products - makes_reactants
    else:
        return 0


def weighted_feed_trial(
    model,
    metric,
    rounds=1,
    start_reactions=["Fish_meal,_protein_70%", "Fish_oil,_sardine"],
    biomass_bound=1,
) -> list:
    """
    navn på funksjon: weighted_feed_trial?

    Simulating feed trials with feeds improved by ingredients and chosen based on a given metric.

    Only feed ingredients in the metric are allowed, so make sure to
    metric is used to formulate the objective and to select among the reduced costs.

    however, it is not neccesary to formulate a df separate from ff.sustainability.
    """
    allowed_reactions = start_reactions
    sol_list = list()
    with model as m:
        for r in get_feedtable_reactions(m):
            if r.id in allowed_reactions:
                r.upper_bound = 1000
            else:
                r.upper_bound = 0
        for _ in range(rounds):
            m.objective = {
                m.reactions.get_by_id(k): -v for k, v in metric.to_dict().items()
            }
            m.reactions.Biomass.bounds = biomass_bound, biomass_bound
            sol = m.optimize()
            lim = (
                (sol.reduced_costs[metric.index.to_list()] / metric)
                .sort_values()
                .index[-1]
            )
            m.reactions.get_by_id(lim).upper_bound = 1000
            sol_list.append(sol)
        return sol_list


def plot_simulation_branch(
    sol: cobra.core.Solution, metric: pd.Series, marker: str, colors: dict, ax
):
    """
    Do this for just one line/solution/feed/lipid_composition
    """


    fluxes = sol.fluxes[-321:][sol.fluxes[-321:] > 0]
    solution_df = pd.DataFrame(
        {"gram per gram growth": fluxes, metric.name: fluxes * metric[fluxes.index]}
    )
    solution_df = solution_df.sort_values(by="gram per gram growth", ascending=False)
    solution_df = solution_df.cumsum()
    gpg = solution_df["gram per gram growth"]
    co2 = solution_df[metric.name]
    previousc, previousg = 0, 0
    for idx, g, c in zip(gpg.index, gpg, co2):
        ax.plot((previousg, g), (previousc, c), color=colors[idx], label=idx)
        previousc, previousg = c, g
    ax.scatter(previousg, previousc, marker=marker, color="black")
    plt.ylabel(metric.name)
    plt.xlabel("gram feed per gram growth")


def legend_without_duplicate_labels(ax, loc=None, bbox_to_anchor=None):
    """
    Copied from https://stackoverflow.com/a/56253636
    """
    handles, labels = ax.get_legend_handles_labels()
    unique = [
        (h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]
    ]
    ax.legend(*zip(*unique), loc=loc, bbox_to_anchor=bbox_to_anchor)
