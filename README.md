# Constrain metabolic models in cobrapy using ingredients from [Feedtables](http://www.feedtables.com)  

## To clone as submodule  
`git submodule add git@gitlab.com:firot/feedtables-simulations.git feedtables_simulations`  
Note: python does not like dashes in variables/module names/ect, but it is the convention for git, so replacing it with a underscore helps.  

## Example of usage  
```python
from functions import feed_frame, add_feed_supply_reaction
ff = feed_frame()
for feed in ff.frame.columns[1:]:
    add_feed_supply_reaction(model, ff.frame[feed].fillna(0))
```
